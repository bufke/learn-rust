use clap::{Parser, Subcommand};
use std::time::Duration;
use tokio::task::JoinSet;
use tokio::{task, time};

use state::get_state;
use worker::cleanup_tasks::cleanup_old_events;
use worker::worker_app::{get_celery_app, get_celery_beat};

pub mod conf;
pub mod entities;
pub mod errors;
pub mod state;
pub mod storage;
pub mod tests;
pub mod web;
pub mod worker;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    RunServer {},
    CeleryWorker {},
    CeleryBeat {},
}

// Proof of concept for running without redis/celery
async fn run_tasks() {
    let forever = task::spawn(async {
        let mut interval = time::interval(Duration::from_secs(720));

        loop {
            interval.tick().await;
            task::spawn(async {
                let _ = cleanup_old_events().await;
            });
        }
    });

    let _ = forever.await;
}

#[tokio::main]
async fn main() {
    env_logger::init();
    let settings = &conf::SETTINGS;
    let _guard = sentry::init(settings.sentry_dsn.clone());
    let cli = Cli::parse();
    match &cli.command {
        Some(Commands::RunServer {}) => {
            let web_app = web::web_server::get_web_app(get_state().await);
            println!("Running GlitchTip on localhost:3000");
            axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
                .serve(web_app.into_make_service())
                .await
                .unwrap();
        }
        Some(Commands::CeleryWorker {}) => {
            let celery_app = get_celery_app().await;
            celery_app.display_pretty().await;
            celery_app.consume().await.unwrap();
        }
        Some(Commands::CeleryBeat {}) => {
            let mut beat = get_celery_beat().await;
            beat.start().await.unwrap();
        }
        None => {
            let mut set = JoinSet::new();

            set.spawn(async move { run_tasks().await });
            set.spawn(async move {
                let web_app = web::web_server::get_web_app(get_state().await);
                println!(
                    "Running all-in-one GlitchTip on localhost:3000 with embedded task scheduler"
                );
                axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
                    .serve(web_app.into_make_service())
                    .await
                    .unwrap();
            });
            set.join_next().await;
        }
    }
}
