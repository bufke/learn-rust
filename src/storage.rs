use crate::conf;
use object_store::{aws::AmazonS3Builder, ObjectStore};
use std::sync::Arc;

fn get_object_store() -> Arc<dyn ObjectStore> {
    let settings = &conf::SETTINGS;
    let s3 = AmazonS3Builder::new()
        .with_access_key_id("")
        .with_secret_access_key("")
        .with_endpoint("https://nyc3.digitaloceanspaces.com")
        .with_bucket_name("assets-staging-glitchtip")
        .with_region(&settings.aws_s3_region_name)
        .build()
        .expect("error creating s3");
    Arc::new(s3)
}

pub fn get_storage() -> Arc<dyn ObjectStore> {
    let storage: Arc<dyn ObjectStore> = get_object_store();
    storage
}
