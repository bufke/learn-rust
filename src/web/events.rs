use axum::http::StatusCode;
use axum::{
    extract::Path,
    extract::{self, State},
    routing::post,
    Json, Router,
};
use sea_orm::{
    ActiveValue, ColumnTrait, DatabaseConnection, EntityTrait, FromQueryResult, JoinType,
    QueryFilter, QuerySelect, RelationTrait,
};
use serde_json::{json, Value};
use time::OffsetDateTime;
use uuid::Uuid;

use crate::entities::issues_issue;
use crate::web::models::ExceptionValues;
use crate::{entities::projects_projectkey, state::AppState};
use crate::{
    entities::{organizations_ext_organization, projects_project},
    errors::{bad_request, internal_error},
};
use serde::{Deserialize, Serialize};

use super::models::{Exception, NewErrorEvent};

pub(crate) fn routes() -> Router<AppState> {
    Router::new().route("/api/:project_id/envelope/", post(envelope_store))
}

#[derive(Serialize)]
struct EventResp {
    id: String,
}

#[derive(Debug, Clone, Copy)]
enum EventType {
    Default = 0,
    Error = 1,
    CSP = 2,
    Transaction = 3,
}

#[derive(Deserialize, Debug)]
struct StoreAuth {
    sentry_key: String,
}

#[derive(FromQueryResult, Debug)]
struct ProjectAccepting {
    name: String,
    is_accepting_events: bool,
}

fn generate_uuid() -> Uuid {
    Uuid::new_v4()
}

#[derive(Debug, Deserialize)]
struct NewEventPayload {
    #[serde(default = "generate_uuid")]
    event_id: Uuid,
    #[serde(flatten)]
    data: Value,
}

async fn envelope_store(
    state: State<AppState>,
    Path(project_id): Path<i64>,
    query_string: extract::Query<StoreAuth>,
    extract::Json(payload): extract::Json<NewEventPayload>,
) -> Result<Json<EventResp>, (StatusCode, String)> {
    let auth: StoreAuth = query_string.0;
    let public_key = Uuid::parse_str(&auth.sentry_key).map_err(bad_request)?;
    let conn = &state.db;
    let res: Option<ProjectAccepting> = projects_project::Entity::find_by_id(project_id)
        .column_as(
            organizations_ext_organization::Column::IsAcceptingEvents,
            "is_accepting_events",
        )
        .join(
            JoinType::InnerJoin,
            projects_project::Relation::OrganizationsExtOrganization.def(),
        )
        .join(
            JoinType::InnerJoin,
            projects_project::Relation::ProjectsProjectkey.def(),
        )
        .filter(projects_projectkey::Column::PublicKey.eq(public_key))
        .into_model::<ProjectAccepting>()
        .one(conn)
        .await
        .map_err(internal_error)?;

    if res.is_none() {
        return Err((StatusCode::NOT_FOUND, String::from("Not found")));
    } else if res.is_some_and(|project_accepting| !project_accepting.is_accepting_events) {
        return Err((
            StatusCode::TOO_MANY_REQUESTS,
            String::from("event rejected due to rate limit"),
        ));
    }

    let resp = EventResp {
        id: payload.event_id.to_string(),
    };

    tokio::spawn(async move {
        injest_event(state.db.clone(), project_id, payload).await;
    });

    Ok(Json(resp))
}

async fn injest_event(db: DatabaseConnection, project_id: i64, event: NewEventPayload) {
    let mut event_type = EventType::Default;
    let mut title = "<unknown>".to_string();
    let mut culprit = String::new();

    if let Some(exception) = event.data.get("exception") {
        event_type = EventType::Error;

        let error_event = NewErrorEvent {
            event_id: event.event_id,
            exception: serde_json::from_value::<ExceptionValues>(exception.to_owned()).unwrap(),
            platform: event.data.get("platform").cloned(),
            logentry: event.data.get("logentry").cloned(),
            message: event.data.get("message").cloned(),
        };
        title = get_exception_title(&error_event.exception.values);
    } else if event.data.get("platform").is_none() {
        event_type = EventType::CSP;
    }
    let now = OffsetDateTime::now_utc();

    let issue = issues_issue::ActiveModel {
        created: ActiveValue::Set(now),
        culprit: ActiveValue::Set(Some(culprit)),
        has_seen: ActiveValue::Set(true),
        is_public: ActiveValue::Set(false),
        level: ActiveValue::Set(2),
        metadata: ActiveValue::Set(json!({"key1": "value1", "key2": "value2"})),
        tags: ActiveValue::Set(json!({"tag1": "value1", "tag2": "value2"})),
        project_id: ActiveValue::Set(project_id),
        title: ActiveValue::Set(title),
        r#type: ActiveValue::Set(event_type as i16),
        status: ActiveValue::Set(0),
        short_id: ActiveValue::Set(Some(123)),
        count: ActiveValue::Set(5),
        last_seen: ActiveValue::Set(now),
        ..Default::default()
    };

    let res = issues_issue::Entity::insert(issue).exec(&db).await.unwrap();

    println!("res: {:?}", res);
    println!("Event Type: {:?}", event_type);
}

fn get_exception_title(exceptions: &Vec<Exception>) -> String {
    if let Some(first_exception) = exceptions.first() {
        return format!("{}: {}", first_exception.typ, first_exception.value);
    }
    "unknown".to_string()
}
