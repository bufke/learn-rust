use axum::{extract::State, http::StatusCode, routing::get, Json, Router};
use sea_orm::{entity::*, query::*};
use serde::Serialize;

use crate::conf::{self, Settings};
pub use crate::entities::socialaccount_socialapp::Model as SocialApp;
use crate::errors::internal_error;
use crate::state::AppState;

pub(crate) fn routes() -> Router<AppState> {
    Router::new().route("/api/settings/", get(api_settings))
}

async fn is_user_registration_open(
    settings: &Settings,
    state: State<AppState>,
) -> Result<bool, (StatusCode, String)> {
    use crate::entities::users_user::Entity as User;

    if settings.enable_user_registration {
        return Ok(true);
    }

    let users = User::find()
        .limit(1)
        .all(&state.db)
        .await
        .map_err(internal_error)?;
    Ok(!users.is_empty())
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct SettingsResponse {
    social_apps: Vec<SocialApp>,
    billing_enabled: bool,
    i_paid_for_glitchtip: bool,
    enable_user_registration: bool,
    enable_organization_creation: bool,
    stripe_public_key: Option<String>,
    plausible_url: Option<String>,
    plausible_domain: Option<String>,
    chatwoot_website_token: Option<String>,
    sentry_dsn: Option<String>,
    sentry_traces_sample_rate: f64,
    environment: Option<String>,
    version: String,
    server_time_zone: String,
}

async fn api_settings(
    state: State<AppState>,
) -> Result<Json<SettingsResponse>, (StatusCode, String)> {
    use crate::entities::socialaccount_socialapp::Entity as SocialApp;

    let social_apps = SocialApp::find()
        .all(&state.db)
        .await
        .map_err(internal_error)?;

    let settings = &conf::SETTINGS;
    let open_registration = is_user_registration_open(settings, state).await.unwrap();

    let response = SettingsResponse {
        social_apps,
        i_paid_for_glitchtip: settings.i_paid_for_glitchtip,
        billing_enabled: settings.billing_enabled,
        enable_user_registration: open_registration,
        enable_organization_creation: settings.enable_user_registration,
        stripe_public_key: settings.stripe_public_key.clone(),
        plausible_domain: settings.plausible_domain.clone(),
        plausible_url: settings.plausible_url.clone(),
        chatwoot_website_token: settings.chatwoot_website_token.clone(),
        sentry_dsn: settings.sentry_frontend_dsn.clone(),
        sentry_traces_sample_rate: settings.sentry_traces_sample_rate,
        environment: settings.environment.clone(),
        version: settings.version.clone(),
        server_time_zone: settings.time_zone.clone(),
    };

    Ok(Json(response))
}
