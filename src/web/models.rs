use serde::Deserialize;
use serde_json::Value;
use uuid::Uuid;

#[derive(Debug, Deserialize)]
pub struct ExceptionMechanism {
    #[serde(rename = "type")]
    pub typ: String,
    pub description: Option<String>,
    pub help_link: Option<String>,
    pub handled: Option<bool>,
    pub synthetic: Option<bool>,
    pub meta: Option<serde_json::Value>,
    pub data: Option<serde_json::Value>,
}

#[derive(Debug, Deserialize)]
pub struct Exception {
    #[serde(rename = "type")]
    pub typ: String,
    pub value: String,
    pub module: Option<String>,
    pub thread_id: Option<String>,
    pub mechanism: Option<ExceptionMechanism>,
    pub stacktrace: Option<serde_json::Value>,
}

#[derive(Debug, Deserialize)]
pub struct ExceptionValues {
    pub values: Vec<Exception>,
}

#[derive(Deserialize, Debug)]
pub struct NewErrorEvent {
    pub event_id: Uuid,
    pub exception: ExceptionValues,
    pub platform: Option<Value>,
    pub logentry: Option<Value>,
    pub message: Option<Value>,
}
