use std::sync::Arc;
use std::time::Duration;

use object_store::ObjectStore;
use sea_orm::{ConnectOptions, Database, DatabaseConnection};

use crate::conf;
use crate::storage;

#[derive(Clone)]
pub struct AppState {
    pub db: DatabaseConnection,
    pub storage: Arc<dyn ObjectStore>,
}

pub async fn get_state() -> AppState {
    let settings = &conf::SETTINGS;
    let mut opt = ConnectOptions::new(settings.database_url.clone());
    opt.max_connections(20)
        .min_connections(2)
        .connect_timeout(Duration::from_secs(8))
        .acquire_timeout(Duration::from_secs(8))
        .idle_timeout(Duration::from_secs(8))
        .max_lifetime(Duration::from_secs(8));
    let db = Database::connect(opt)
        .await
        .expect("Database connection failure");

    let storage = storage::get_storage();

    let state = AppState { db, storage };
    state
}
