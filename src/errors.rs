use axum::http::StatusCode;

pub fn internal_error<E>(err: E) -> (StatusCode, String)
where
    E: std::error::Error,
{
    println!("500 error: {:?}", err.to_string());
    (StatusCode::INTERNAL_SERVER_ERROR, String::from("Error"))
}

pub fn not_found<E>(_: E) -> (StatusCode, String)
where
    E: std::error::Error,
{
    (StatusCode::NOT_FOUND, String::from("Not Found"))
}

pub fn bad_request<E>(err: E) -> (StatusCode, String)
where
    E: std::error::Error,
{
    (StatusCode::BAD_REQUEST, err.to_string())
}
